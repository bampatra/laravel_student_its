<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketSubmit extends Model
{
    protected $fillable = [
        'name', 'email' ,'phone',  'opsystype','type', 'desc','escalation', 'priority', 'comment'
    ];
}
