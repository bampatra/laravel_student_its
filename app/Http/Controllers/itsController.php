<?php

namespace App\Http\Controllers;

use App\TicketSubmit;
use Illuminate\Http\Request;
use App\its;

class itsController extends Controller
{
//    public function create()
//    {
//        $ticket = TicketSubmit::all();
//        return view('its', ['ticket' => $ticket ]);
//    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'comment' => 'required|Max:200',
        ]);


//        $allRequest = $request->all();
//
//        $itsticket = its::find($allRequest['id']);
//

        $allRequest = $request->all();

        $itsticket = new its;
        $itsticket->id = $allRequest['id'];
        $itsticket->comment = $allRequest['comment'];
//        if($itsticket->comment!=null) {
//            $itsticket->update();
//        } else {
//            $itsticket->save();
//        }
        $itsticket->save();


//        its::find($id)->update($request->all());
        return redirect()->route('ticket.edit',$allRequest['id']) ->with('success','Comment submitted successfully');
    }

    public function showuser(Request $request, $id)
    {
        $ticket= TicketSubmit::find($id);
       $comment=its::find($id);
       return view('showuser',compact(['ticket','comment']));

//        $ticket= TicketSubmit::orderBy('id','DESC')->paginate(5);
//        $comment= its::orderBy('id','DESC')->paginate(5);
//        return view('showuser',compact(['ticket','comment'])) ->with('i', ($request->input('page', 1) - 1) * 5);
    }

}
