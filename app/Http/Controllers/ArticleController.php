<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TicketSubmit;

class ArticleController extends Controller
{
    //
    public function index()
    {
        return TicketSubmit::all();
    }

    public function show($id)
    {
        return TicketSubmit::find($id);
    }

    public function store(Request $request)
    {
        return TicketSubmit::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $article = TicketSubmit::findOrFail($id);
        $article->update($request->all());

        return $article;
    }

    public function delete(Request $request, $id)
    {
        $article = TicketSubmit::findOrFail($id);
        $article->delete();

        return 204;
    }

}
