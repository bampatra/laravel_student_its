<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\TicketSubmit;
use App\its;

class itsCRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ticket= TicketSubmit::orderBy('id','DESC')->paginate(5);
        return view('its',compact('ticket')) ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket= TicketSubmit::find($id);
        $comment=its::find($id);
        return view('CRUD.show',compact(['ticket','comment']));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket= TicketSubmit::find($id);
        $comment=its::find($id);
        return view('CRUD.edit',compact(['ticket','comment']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|Min:3|Max:20|Regex:/^[\pL\s]+$/u',
            'email' => 'required|Email|Between:5,64',
            'phone' => 'required|Regex:/^([0-9 -])+$/i',
            'opsystype' => 'required|Min:3|Max:20|Alpha' ,
            'type' => 'required|Min:3|Max:20|Alpha',
            'desc' => 'required|Max:200',
        ]);
        TicketSubmit::find($id)->update($request->all());

        return redirect()->route('ticket.edit',$id) ->with('success','Status updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TicketSubmit::find($id)->delete();
        return redirect()->route('ticket.index') ->with('success','Product deleted successfully');
    }

}
