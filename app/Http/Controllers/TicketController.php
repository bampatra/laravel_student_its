<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\TicketSubmit;
use App\Http\Requests\Validation;
class TicketController extends Controller
{

    //this function authenticates user when the following functions are called
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $ticket = new TicketSubmit;
        return view('request', ['ticket' => $ticket ]);
    }

    public function progress(Request $request){
        $ticket= TicketSubmit::orderBy('id','DESC')->paginate();
        return view('trackprogress',compact('ticket')) ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function store(Validation $request)
    {
//        $this->validate($request, [
//            'name' => 'required|Min:3|Max:20|Regex:/^[\pL\s]+$/u',
//            'email' => 'required|Email|Between:5,64',
//            'phone' => 'required|Regex:/^([0-9 -])+$/i',
//            'opsystype' => 'required|Min:3|Max:20|Alpha' ,
//            'type' => 'required|Min:3|Max:20|Regex:/^[\pL\s]+$/u',
//            'desc' => 'required|Max:200',
//        ]);
        TicketSubmit::create($request->all());
        return redirect()->route('request_store') ->with('success','Ticket submitted successfully');

    }
}