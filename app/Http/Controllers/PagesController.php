<?php

namespace App\Http\Controllers;
use App\TicketSubmit;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(){
        return view('itshome');
    }

    public function faq(){
        return view('faq'); //will return the view file of faq.blade
    }

    public function its(){
        return view('its');
    }


}
