<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\its;
use App\TicketSubmit;

class CommentController extends Controller
{
    public function storeComment(Request $request)
    {
        return its::create($request->all());
    }

    public function updateComment(Request $request, $id)
    {
        $comment = TicketSubmit::findOrFail($id);
        $comment->update($request->all());

        return $comment;
    }
}
