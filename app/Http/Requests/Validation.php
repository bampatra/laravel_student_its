<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Validation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|Min:3|Max:20|Regex:/^[\pL\s]+$/u',
            'email' => 'required|Email|Between:5,64',
            'phone' => 'required|Regex:/^([0-9 -])+$/i',
            'opsystype' => 'required|Min:3|Max:20|Alpha' ,
            'type' => 'required|Min:3|Max:20|Alpha',
            'desc' => 'required|Max:200',
        ];
    }
}
