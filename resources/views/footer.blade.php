<footer class="well-sm navbar navbar-absolute-bottom">
    <div class="row">
        <div class="branded-footer-left col-xs-12">Copyright © 2016 RMIT University |
            <a href="http://www.rmit.edu.au/utilities/disclaimer/" target="_blank">Disclaimer</a> |
            <a href="http://www.rmit.edu.au/utilities/privacy/" target="_blank">Privacy</a> |
            <a href="http://www.rmit.edu.au/utilities/website-feedback/" target="_blank">Website feedback</a> |
            ABN 49 781 030 034 | CRICOS provider number: 00122A
        </div>
    </div>
</footer>