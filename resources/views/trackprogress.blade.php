@extends('master')
@section('title', 'Tracking Progress')
@section('completepage')
    <li><a href="/assignment1ver2/public/request">REQUEST A SERVICE</a></li>
    <li><a href="#">REPORT AN ISSUE</a></li>
    <li><a href="#">FEEDBACK</a></li>
@endsection
@section('progresspage')
    class="active"
@endsection
@section('content')

        
            <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><h4>Tracking Progress</h4></div>
                    <div class="panel-body">
                        </div>
        <div class="container-fluid">
            {{--<div class="jumbotron">--}}
                    <!-- Table -->
                    <table class="table">
                           <tr>
                                   <th>Ticket Number</th>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Description</th>
                                   <th>Status</th>
                                    <th></th>
                               </tr>

                @foreach($ticket as $its)
                            <tr>
                                    <td>{{$its->id}}</td>
                                    <td>{{$its->name}}</td>
                                    <td>{{$its->email}}</td>
                                    <td>{{$its->desc}}</td>
                                    <td>{{$its->type}}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('showuser',$its->id) }}"}>Show</a>
                                    </td>
                                </tr>
                @endforeach

                        </table>
                </div>
    {{--</div>--}}
            </div>

@endsection