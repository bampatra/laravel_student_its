@extends('master')
@section('title', "Ticket Submission ")
@section('completepage')
    <li class="active"><a href="/assignment1ver2/public/request">REQUEST A SERVICE</a></li>
    <li><a href="#">REPORT AN ISSUE</a></li>
    <li><a href="#">FEEDBACK</a></li>
    @endsection
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    {!! Form::model($ticket, ['route' => 'request_store']) !!}

    <div class="container">
                <h2>Request A Service</h2>
                <div class="jumbotron">
                        <div class="form-group">
                                <h3>Name:</h3>
                                {!! Form::text('name', '', ['class' => 'form-control']) !!}
                            <br> </div>

                        <div class="form-group">
                                <h3>E-mail:</h3>
                                  {!! Form::text('email', '', ['class' => 'form-control']) !!}
                                <br></div>

                        <div class="form-group">
                                <h3>Tel:</h3>
                                    {!! Form::text('phone', '', ['class' => 'form-control']) !!}
                                <br> </div>

                        <div class="form-group">
                                <h3>Operating System ( e.g MAC or Windows ):</h3>
                                    {!! Form::text('opsystype', '', ['class' => 'form-control']) !!}
                                <br> </div>

                        {!! Form::hidden('type', 'Pending', ['class' => 'form-control']) !!}
                        {!! Form::hidden('escalation', '0', ['class' => 'form-control']) !!}
                        {!! Form::hidden('priority', 'Not set', ['class' => 'form-control']) !!}
                        {!! Form::hidden('comment', 'No comment yet', ['class' => 'form-control']) !!}


                         <div class="form-group">
                               <h3>Description:</h3>
                            {!! Form::textarea('desc', '', ['class' => 'form-control']) !!}
                                    </div>
                                <br>
                                <button class="btn btn-default" align="right" >Submit</button>



                        </form>
                    </div>
                </div>




    {!! Form::close() !!}
@endsection