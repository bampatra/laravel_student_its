@extends('master')
@section('title', 'Home')
@section('homepage')
    class="active"
@endsection
@section('content')
    <div class="container">

        <div class="jumbotron">
            <h1>Hello there!</h1>
            <p> Information Technology Services (ITS) provides RMIT University with information and communication
                technology in support of RMIT’s research, learning teaching and administrative activities</p>
            {{--<table>--}}
                {{--<tr>--}}
                    {{--<td><a class="btn btn-primary btn-lg btn-group-justified"  href="#" role="button">Request a Service</a></td>--}}
                    {{--<td><a class="btn btn-primary btn-lg btn-group-justified"  href="#" role="button">Report an Issue</a></td>--}}
                    {{--<td><a class="btn btn-primary btn-lg btn-group-justified" href="#" role="button">Track Progress</a></td>--}}


                {{--</tr>--}}
            {{--</table>--}}
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>Request a Service <span class="glyphicon glyphicon-shopping-cart btn-lg" aria-hidden="true"></span> </h3>
                            <p>Need something? We are here to help!</p>
                            <p><a href="/assignment1ver2/public/request" class="btn btn-primary" role="button">REQUEST</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>Report an Issue <span class="glyphicon glyphicon-wrench btn-lg" aria-hidden="true"></span></h3>
                            <p> Let us know if you found any issues </p>
                            <p><a href="#" class="btn btn-primary" role="button">REPORT</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>Feedback <span class="glyphicon glyphicon-refresh btn-lg" aria-hidden="true"></span></h3>
                            <p>We need your feedback. Tell us how we do!</p>
                            <p><a href="#" class="btn btn-primary" role="button">FEEDBACK</a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection