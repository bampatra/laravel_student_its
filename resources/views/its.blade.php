@extends('master')
@section('title', 'ITS')
@section('completepage')
    <li><a href="/assignment1ver2/public/request">REQUEST A SERVICE</a></li>
    <li><a href="#">REPORT AN ISSUE</a></li>
    <li><a href="#">FEEDBACK</a></li>
@endsection
@section('itspage')
    class="active"
@endsection
@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

        <div class="panel panel-default">
                <div class="panel-heading">
                        <h3 class="panel-title">Database</h3>
                    </div>
                <div class="panel-body">

                    <table class="table">
                        <tr>
                            <th> ID </th>
                            <th> Name </th>
                            {{--<th> Email </th>--}}
                            {{--<th> Tel </th>--}}
                            {{--<th> Operating System </th>--}}
                            {{--<th> Type</th>--}}
                            <th> Description </th>
                            <th>Status</th>
                            <th width="280px">Action</th>

                        </tr>
                        @foreach($ticket as $its)
                        {{--<tr>--}}

                            {{--<td> {{$its->id}} </td>--}}
                            {{--<td> {{$its->name}} </td>--}}
                            {{--<td> {{$its->email}} </td>--}}
                            {{--<td> {{$its->phone}} </td>--}}
                            {{--<td> {{$its->opsystype}} </td>--}}
                            {{--<td> {{$its->type}} </td>--}}
                            {{--<td> {{$its->desc}} </td>--}}

                        {{--</tr>--}}
                            <tr>
                                <td>{{$its->id}}</td>
                                <td>{{$its->name}}</td>
                                <td>{{$its->desc}}</td>
                                <td>{{$its->type}}</td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('ticket.show',$its->id) }}"}>Show</a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['ticket.destroy', $its->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </table>

                 </div>


            </div>





@endsection