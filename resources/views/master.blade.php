<html>
<head>
        <title> @yield('title') </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

        <div class="media-left">
                <a href="#">
                        <img
                    class="media-object" src="http://www.simge.edu.sg/pub/groups/webcontentmanager/documents/sim_university_logo/c2lt/mdaw/~edisp/sim000610.png" alt="">
                    </a>
            </div>
@include('shared.navbar')
@yield('content')
@include('footer')
</body>
</html>