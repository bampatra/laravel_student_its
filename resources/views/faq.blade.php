@extends('master')
@section('title', 'FAQ')
@section('faqpage')
    class="active"
@endsection

@section('content')
    <div class="container">
    <p><h2>Frequently Asked Questions</h2></p>
    <div class="list-group">
        <a href="#" class="list-group-item active">
            <h4 class="list-group-item-heading">How do i visit myDesktop?</h4>
            <p class="list-group-item-text">Log in to myRMIT and there’s is a list of link at the right bottom, there’s is a myDesktop button. </p>
        </a>
    </div>

    <div class="list-group">
        <a href="#" class="list-group-item active">
            <h4 class="list-group-item-heading">How do i print paper in RMIT?</h4>
            <p class="list-group-item-text">
                Follow these steps and it’s done: <br>
                1. Send your document to be printed (use myDesktop if you are printing from your own device)<br>
                2. Go to a Ricoh multi-function device<br>
                3. Register your card. Already registered? Go to step 4.<br>
                4. Swipe your student card on the printer and print.<br>

            </p>
        </a>
    </div>

    <div class="list-group">
        <a href="#" class="list-group-item active">
            <h4 class="list-group-item-heading">
                How do i report an incident?
            </h4>
            <p class="list-group-item-text">If you see something suspicious, it’s your responsibility to report it.
                Whether it’s unusual system activity you’ve noticed, a password breach or a suspected hacking attempt,
                the IT Security team are your first call in providing advice and taking action.
                Prompt reporting ensures that potential threats are isolated and steps can be taken to limit the potential impact to students,
                colleagues and the university.<br><br>
            <h4>For urgent incidents and advice</h4>
            Service and Support Centre: Phone +61 3 992 58888<br>
            <h4>For non-urgent requests and queries</h4>
            Please drop down a email to its.csit.rmit.edu.au
            </p>
        </a>
    </div>
    </div>

@endsection