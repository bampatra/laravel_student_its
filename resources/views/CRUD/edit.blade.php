@extends('master')
@section('itspage')
    class="active"
    @endsection
@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="container">
        <div class="jumbotron">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Answer Query</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('ticket.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($ticket, ['method' => 'PATCH','route' => ['ticket.update', $ticket->id]]) !!}
    @include('CRUD.form')
    {!! Form::close() !!}


{{--    {!! Form::model($comment, ['method' => 'POST','route' => 'its.store']) !!}--}}
    {!! Form::model($comment, ['method' => 'POST','route' => 'its.store']) !!}

      <div class="form-group">
       <strong>Comment:</strong>
{{--          {!! Form::textarea('comment', null, array('placeholder' => 'Comment','class' => 'form-control')) !!}--}}
          {!! Form::textarea('comment', '', ['class' => 'form-control']) !!}
        </div>

            {!! Form::hidden('id',  $ticket->id, ['class' => 'form-control']) !!}
        <button type="submit" class="btn btn-primary">Submit Comment</button>
    {!! Form::close() !!}
        </div>
    </div>


@endsection