@extends('master')
@section('itspage')
    class="active"
@endsection
@section('content')
    <div class="container">
     <div class="jumbotron">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Query</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('ticket.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $ticket->name}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $ticket->email}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Phone:</strong>
                {{ $ticket->phone}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Operating System:</strong>
                {{ $ticket->opsystype}}
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $ticket->desc}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Enquiry Status:</strong>
                {{ $ticket->type}}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Comment:</strong>
                @if ($ticket->comment != 'No comment yet')
                    {{--@foreach($comment as $comments)--}}
                    {{ $ticket->comment}}


                    {{--@endforeach--}}
                @else
                    <p> *No comment yet*</p>
                @endif

            </div>
        </div>
        <a class="btn btn-warning" href="{{ route('ticket.edit',$ticket->id) }}">Edit</a>
    </div>
     </div>
    </div>
@endsection