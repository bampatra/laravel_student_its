<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $ticket->name}}
             {!! Form::hidden('name',  $ticket->name, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {{ $ticket->email}}
            {!! Form::hidden('email',  $ticket->email, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Phone:</strong>
            {{ $ticket->phone}}
            {!! Form::hidden('phone',  $ticket->phone, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Operating System:</strong>
            {{ $ticket->opsystype}}
            {!! Form::hidden('opsystype',  $ticket->opsystype, ['class' => 'form-control']) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Description:</strong>
            {{ $ticket->desc}}
            {!! Form::hidden('desc',  $ticket->desc, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Enquiry Status:</strong>
            {!! Form::text('type', null, array('placeholder' => 'Enquiry Status','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <button type="submit" class="btn btn-primary">Update Status</button>
    </div>
</div>