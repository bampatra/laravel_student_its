<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => 'cors'], function () {

    // List
    Route::get('articles/list', 'ArticleController@index');

    // Show
    Route::get('articles/{id}', 'ArticleController@show');

    // Store
    Route::post('articles', 'ArticleController@store');

    // Update
    Route::put('articles/{id}/update', 'ArticleController@update');

    // Delete
    Route::get('articles/{id}/delete', 'ArticleController@delete');

    // Add Comment
    Route::post('articles/{id}/storeComment', 'CommentController@storeComment');

    // Update Comment
    Route::post('articles/{id}/updateComment', 'CommentController@updateComment');

});